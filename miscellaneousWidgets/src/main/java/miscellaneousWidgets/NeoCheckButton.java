package miscellaneousWidgets;

import java.io.IOException;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ObjectPropertyBase;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

/**
  * Custom CheckButton
  */
public class NeoCheckButton extends AnchorPane {
    /** The button */
    @FXML protected Button button;
    /** The icon */
    @FXML protected ImageView icon;
    private int userValue;
    private Image checked = new Image(getClass().getResource("/miscellaneousWidgets/images/check.png").toString());
    private Image unchecked = new Image(getClass().getResource("/miscellaneousWidgets/images/uncheck.png").toString());
    private BooleanProperty checkedProperty = new SimpleBooleanProperty(false);

    private boolean autoCheckMode = true;

    /**
     * Constructor
     */
    public NeoCheckButton() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/miscellaneousWidgets/fxml/NeoCheckButton.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        
        try {
            fxmlLoader.load();            
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        
        checkedProperty.set(false);
    }

    /**
     * AutoCheckMode setter
     * 
     * @param autoCheckMode true if the button is automatically checked when clicked
     */
    public void setAutoCheckMode(boolean autoCheckMode) {
        this.autoCheckMode = autoCheckMode;
    }
    
    /**
     * Text getter
     * 
     * @return text
     */
    public String getText() {
        return button.getText();
    }
    
    /**
     * Text setter
     * 
     * @param value the text
     */
    public void setText(String value) {
        button.setText(value);
    }

    /**
     * UserValue setter
     * 
     * @param value the value
     */
    public void setUserValue(int value) {
        userValue=value;
    }

    /**
     * UserValue getter
     * 
     * @return the value
     */
    public int getUserValue() {
        return userValue;
    }
    
    /**
     * Checker on click depending on the autocheck mode
     */
    @FXML
    protected void buttonClicked() {
        if (autoCheckMode) {
            if (checkedProperty.get()) {
                uncheck();
            } else {
                check();
            }
        }
    }

    /**
     * Check the button
     */
    public void check() {
        icon.setImage(checked);
        checkedProperty.set(true);
    }

    /**
     * Uncheck the button
     */
    public void uncheck() {
        icon.setImage(unchecked);
        checkedProperty.set(false);
    }

    /**
     * Checked property getter
     * 
     * @return the checked property
     */
    public BooleanProperty getCheckedProperty() {
        return checkedProperty;
    }

    /**
     * Check getter
     * 
     * @return a boolean value
     */
    public boolean isChecked() {
        return checkedProperty.get();
    }

    /**
     * Event handler for action
     * 
     * @return the object property holding the event handler
     */
    public final ObjectProperty<EventHandler<ActionEvent>> onActionProperty() { return onAction; }

    /**
     * Action setter
     * 
     * @param value : the event handler
     */
    public final void setOnAction(EventHandler<ActionEvent> value) { onActionProperty().set(value); }

    /**
     * Action getter
     * 
     * @return the event handler
     */
    public final EventHandler<ActionEvent> getOnAction() { return onActionProperty().get(); }

    /**
     * Object property for the action
     */
    private ObjectProperty<EventHandler<ActionEvent>> onAction = new ObjectPropertyBase<EventHandler<ActionEvent>>() {
        @Override protected void invalidated() {
            setEventHandler(ActionEvent.ACTION, get());
        }

        @Override
        public Object getBean() {
            return this;
        }

        @Override
        public String getName() {
            return "onAction";
        }
    };
}
