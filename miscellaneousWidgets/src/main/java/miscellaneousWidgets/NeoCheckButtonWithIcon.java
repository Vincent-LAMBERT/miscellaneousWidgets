package miscellaneousWidgets;



import java.io.IOException;

import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ObjectPropertyBase;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.StringProperty;
import javafx.css.CssMetaData;
import javafx.css.StyleableStringProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;

/**
 * A custom checkbutton with image at its left
 */
public class NeoCheckButtonWithIcon extends AnchorPane {
    /** The button */
    @FXML protected Button btn;
    /** The label */
    @FXML protected Label label;
    /** The anchor pane */
    @FXML protected AnchorPane internAnch;
    /** The image anchor pane */
    @FXML protected AnchorPane imgAnch;
    /** The icon */
    @FXML protected ImageView icon;
    /** The hbox */
    @FXML protected HBox hbox;
    /** The check icon */
    @FXML protected ImageView checkIcon;

    private int userValue;
    private Image checked = new Image(getClass().getResource("/miscellaneousWidgets/images/check.png").toString());
    private Image unchecked = new Image(getClass().getResource("/miscellaneousWidgets/images/uncheck.png").toString());
    private BooleanProperty checkedProperty = new SimpleBooleanProperty(false);
    private boolean autoCheckMode = true;


    /**
     * Constructor
     */
    public NeoCheckButtonWithIcon() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/miscellaneousWidgets/fxml/NeoCheckButtonWithIcon.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        
        try {
            fxmlLoader.load();            
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        DoubleBinding db = new DoubleBinding() {
            {
                super.bind(imgAnch.widthProperty(), btn.widthProperty());
            }
            @Override
            protected double computeValue() {
                return (btn.widthProperty().get() - imgAnch.widthProperty().get());
            }
        };
        icon.fitHeightProperty().bind(btn.heightProperty().subtract(26));
        icon.fitWidthProperty().bind(btn.heightProperty().subtract(26));
        label.minWidthProperty().bind(db.divide(1.25));
        label.minHeightProperty().bind(btn.heightProperty().multiply(1).subtract(4));
        
        checkedProperty.set(false);
    }

    /**
     * AutoCheckMode setter
     * 
     * @param autoCheckMode true if the button is automatically checked when clicked
     */
    public void setAutoCheckMode(boolean autoCheckMode) {
        this.autoCheckMode = autoCheckMode;
    }
    
    
    /**
     * Check the button depending on the autoCheckMode
     */
    @FXML
    protected void buttonClicked() {
        if (autoCheckMode) {
            if (checkedProperty.get()) {
                uncheck();
            } else {
                check();
            }
        }
    }

    /**
     * UserValue setter
     * 
     * @param value the value to set
     */
    public void setUserValue(int value) {
        userValue=value;
    }

    /**
     * UserValue getter
     * 
     * @return the value
     */
    public int getUserValue() {
        return userValue;
    }

    /**
     * Check the button
     */
    public void check() {
        checkIcon.setImage(checked);
        checkedProperty.set(true);
    }

    /**
     * Uncheck the button
     */
    public void uncheck() {
        checkIcon.setImage(unchecked);
        checkedProperty.set(false);
    }

    /**
     * Checked property getter
     * 
     * @return the checked property
     */
    public BooleanProperty getCheckedProperty() {
        return checkedProperty;
    }

    /**
     * Check if the button is checked
     * 
     * @return true if the button is checked
     */
    public boolean isChecked() {
        return checkedProperty.get();
    }
    
    /**
     * Text getter
     * 
     * @return the text
     */
    public String getText() {
        return label.getText();
    }
    
    /**
     * Text setter
     * 
     * @param value the text
     */
    public void setText(String value) {
        label.setText(value);
    }

    /**
     * Event handler for the action
     * 
     * @return the object property holding the event handler
     */
    public final ObjectProperty<EventHandler<ActionEvent>> onActionProperty() { return onAction; }

    /**
     * Action setter
     * 
     * @param value the event handler
     */
    public final void setOnAction(EventHandler<ActionEvent> value) { onActionProperty().set(value); }

    /**
     * Action getter
     * 
     * @return the event handler
     */
    public final EventHandler<ActionEvent> getOnAction() { return onActionProperty().get(); }

    /**
     * Object property for the action
     */
    private ObjectProperty<EventHandler<ActionEvent>> onAction = new ObjectPropertyBase<EventHandler<ActionEvent>>() {
        @Override protected void invalidated() {
            setEventHandler(ActionEvent.ACTION, get());
        }

        @Override
        public Object getBean() {
            return btn;
        }

        @Override
        public String getName() {
            return "onAction";
        }
    };

    /**
     * Image property
     */
    private ObjectProperty<Image> image;

    /**
     * Image setter
     * 
     * @param image the image
     */
    public final void setImage(Image image) {
        icon.setImage(image);
    }
    /**
     * Image getter
     * 
     * @return the image
     */
    public final Image getImage() {
        return icon.getImage();
    }

    @SuppressWarnings("unused")
    /** The old image */
	private Image oldImage;

    /**
     * Image property setter
     * 
     * @return the object property holding the image
     */
    public final ObjectProperty<Image> imageProperty() {
        if (image == null) {
            image = new ObjectPropertyBase<Image>() {

                @Override
                public Object getBean() {
                    return icon;
                }

                @Override
                public String getName() {
                    return "image";
                }
            };
        }
        return image;
    }

    /** The imageUrl property */
    private StringProperty imageUrl = null;

    /**
     * The imageUrl property is set from CSS and then the image property is
     * set from the invalidated method. This ensures that the same image isn't
     * reloaded.
     * 
     * @return the string property holding the image url
     */
    @SuppressWarnings("unused")
	private StringProperty imageUrlProperty() {
        if (imageUrl == null) {
            imageUrl = new StyleableStringProperty() {
                @Override
                public Object getBean() {
                    return icon;
                }

                @Override
                public String getName() {
                    return "imageUrl";
                }

                @Override
                public CssMetaData<ImageView,String> getCssMetaData() {
                    return null;
                }

            };
        }
        return imageUrl;
    }

}
