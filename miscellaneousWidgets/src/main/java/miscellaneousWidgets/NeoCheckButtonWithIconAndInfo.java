package miscellaneousWidgets;



import java.io.IOException;

import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ObjectPropertyBase;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.StringProperty;
import javafx.css.CssMetaData;
import javafx.css.StyleableStringProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import miscellaneousWidgets.events.MultipleButtonEvent;
import miscellaneousWidgets.events.PrimoEvent;
import miscellaneousWidgets.events.SecondoEvent;

/**
 * A custom checkbutton with image at its left and mini icon at its right
 */
public class NeoCheckButtonWithIconAndInfo extends AnchorPane {
    /** The button */
    @FXML protected Button btn;
    /** The label */
    @FXML protected Label label;
    /** The anchor pane */
    @FXML protected AnchorPane internAnch;
    /** The image anchor */
    @FXML protected AnchorPane imgAnch;
    /** The icon */
    @FXML protected ImageView icon;
    /** The hbox */
    @FXML protected HBox hbox;
    /** The check icon */
    @FXML protected ImageView checkIcon;
    /** The mini button */
    @FXML protected Button miniButton;

    private int userValue;
    private Image checked = new Image(getClass().getResource("/miscellaneousWidgets/images/check.png").toString());
    private Image unchecked = new Image(getClass().getResource("/miscellaneousWidgets/images/uncheck.png").toString());
    private BooleanProperty checkedProperty = new SimpleBooleanProperty(false);
    private boolean autoCheckMode = true;


    /**
     * Constructor
     */
    public NeoCheckButtonWithIconAndInfo() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/miscellaneousWidgets/fxml/NeoCheckButtonWithIconAndInfo.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        
        try {
            fxmlLoader.load();            
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        DoubleBinding db = new DoubleBinding() {
            {
                super.bind(imgAnch.widthProperty(), btn.widthProperty());
            }
            @Override
            protected double computeValue() {
                return (btn.widthProperty().get() - imgAnch.widthProperty().get());
            }
        };
        icon.fitHeightProperty().bind(btn.heightProperty().subtract(26));
        icon.fitWidthProperty().bind(btn.heightProperty().subtract(26));
        label.minWidthProperty().bind(db.divide(1.25));
        label.minHeightProperty().bind(btn.heightProperty().multiply(1).subtract(4));

        checkedProperty.set(false);
    }

    /**
     * AutoCheckMode setter
     * 
     * @param autoCheckMode true if the button is in autocheck mode
     */
    public void setAutoCheckMode(boolean autoCheckMode) {
        this.autoCheckMode = autoCheckMode;
    }

    /**
     * UserValue setter
     * 
     * @param value the value to set
     */
    public void setUserValue(int value) {
        userValue=value;
    }

    /**
     * UserValue getter
     * 
     * @return the value
     */
    public int getUserValue() {
        return userValue;
    }

    /**
     * Check the button
     */
    public void check() {
        checkIcon.setImage(checked);
        checkedProperty.set(true);
    }

    /**
     * Uncheck the button
     */
    public void uncheck() {
        checkIcon.setImage(unchecked);
        checkedProperty.set(false);
    }

    /**
     * Checked property getter
     * 
     * @return the checked property
     */
    public BooleanProperty getCheckedProperty() {
        return checkedProperty;
    }

    /**
     * Check if the button is checked
     * 
     * @return true if the button is checked
     */
    public boolean isChecked() {
        return checkedProperty.get();
    }
    
    /**
     * Text getter
     * 
     * @return the text
     */
    public String getText() {
        return label.getText();
    }
    
    /**
     * Text setter
     * 
     * @param value the text to set
     */
    public void setText(String value) {
        label.setText(value);
    }

    /**
     * Main button handling
     * 
     * @param event the event
     */
    @FXML
    public void handleMainButton(ActionEvent event) {
        if (autoCheckMode) {
            if (checkedProperty.get()) {
                uncheck();
            } else {
                check();
            }
        }
        PrimoEvent multipleButtonEvent = new PrimoEvent();
        this.fireEvent(multipleButtonEvent);
    }

    /**
     * Mini button handling
     * 
     * @param event the event
     */
    @FXML
    public void handleMiniButton(ActionEvent event) {
        SecondoEvent multipleButtonEvent = new SecondoEvent();
        this.fireEvent(multipleButtonEvent);
    }

    /**
     * Event handler for main action
     * 
     * @return the event handler
     */
    public final ObjectProperty<EventHandler<MultipleButtonEvent>> onMainActionProperty() { return onMainAction; }

    /**
     * Main action setter
     * 
     * @param value the event handler
     */
    public final void setOnMainAction(EventHandler<MultipleButtonEvent> value) { onMainActionProperty().set(value); }

    /**
     * Main action getter
     * 
     * @return the event handler
     */
    public final EventHandler<MultipleButtonEvent> getOnMainAction() { return onMainActionProperty().get(); }

    /**
     * Object property for the main action
     */
    private ObjectProperty<EventHandler<MultipleButtonEvent>> onMainAction = new ObjectPropertyBase<EventHandler<MultipleButtonEvent>>() {
        @Override protected void invalidated() {
            setEventHandler(PrimoEvent.ACTION, get());
        }

        @Override
        public Object getBean() {
            return btn;
        }

        @Override
        public String getName() {
            return "onMainAction";
        }
    };

    
    /**
     * Event handler for info action
     * 
     * @return the event handler
     */
    public final ObjectProperty<EventHandler<MultipleButtonEvent>> onInfoActionProperty() { return onInfoAction; }

    /**
     * Info action setter
     * 
     * @param value the event handler
     */
    public final void setOnInfoAction(EventHandler<MultipleButtonEvent> value) { onInfoActionProperty().set(value); }

    /**
     * Info action getter
     * 
     * @return the event handler
     */
    public final EventHandler<MultipleButtonEvent> getOnInfoAction() { return onInfoActionProperty().get(); }

    /**
     * Object property for the info action
     */
    private ObjectProperty<EventHandler<MultipleButtonEvent>> onInfoAction = new ObjectPropertyBase<EventHandler<MultipleButtonEvent>>() {
        @Override protected void invalidated() {
            setEventHandler(SecondoEvent.ACTION, get());
        }

        @Override
        public Object getBean() {
            return miniButton;
        }

        @Override
        public String getName() {
            return "onMiniButtonAction";
        }
    };

    /**
     * Image property
     */
    private ObjectProperty<Image> image;

    /**
     * Image setter
     * 
     * @param image the image to set
     */
    public final void setImage(Image image) {
        icon.setImage(image);
    }
    /**
     * Image getter
     * 
     * @return the image
     */
    public final Image getImage() {
        return icon.getImage();
    }

    @SuppressWarnings("unused")
	private Image oldImage;

    /**
     * Image property setter
     * 
     * @return the image property
     */
    public final ObjectProperty<Image> imageProperty() {
        if (image == null) {
            image = new ObjectPropertyBase<Image>() {

                @Override
                public Object getBean() {
                    return icon;
                }

                @Override
                public String getName() {
                    return "image";
                }
            };
        }
        return image;
    }

    private StringProperty imageUrl = null;

    /**
     * The imageUrl property is set from CSS and then the image property is
     * set from the invalidated method. This ensures that the same image isn't
     * reloaded.
     * 
     * @return the image url property
     */
    @SuppressWarnings("unused")
	private StringProperty imageUrlProperty() {
        if (imageUrl == null) {
            imageUrl = new StyleableStringProperty() {
                @Override
                public Object getBean() {
                    return icon;
                }

                @Override
                public String getName() {
                    return "imageUrl";
                }

                @Override
                public CssMetaData<ImageView,String> getCssMetaData() {
                    return null;
                }

            };
        }
        return imageUrl;
    }

}
