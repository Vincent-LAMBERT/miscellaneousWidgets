package miscellaneousWidgets;



import java.io.IOException;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ObjectPropertyBase;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import miscellaneousWidgets.events.MultipleButtonEvent;
import miscellaneousWidgets.events.PrimoEvent;
import miscellaneousWidgets.events.SecondoEvent;

/**
 * A button with image at its left
 */
public class NeoCheckButtonWithInfo extends AnchorPane {
    /** The button */
    @FXML protected Button btn;
    /** The anchor pane */
    @FXML protected ImageView checkIcon;
    /** The mini button */
    @FXML protected Button miniButton;

    private int userValue;
    private Image checked = new Image(getClass().getResource("/miscellaneousWidgets/images/check.png").toString());
    private Image unchecked = new Image(getClass().getResource("/miscellaneousWidgets/images/uncheck.png").toString());
    private BooleanProperty checkedProperty = new SimpleBooleanProperty(false);
    private boolean autoCheckMode = true;

    /**
     * Constructor
     */
    public NeoCheckButtonWithInfo() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/miscellaneousWidgets/fxml/NeoCheckButtonWithInfo.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        
        try {
            fxmlLoader.load();            
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        checkedProperty.set(false);
    }

    /**
     * AutoCheckMode setter
     * 
     * @param autoCheckMode true if the button is in autocheck mode
     */
    public void setAutoCheckMode(boolean autoCheckMode) {
        this.autoCheckMode = autoCheckMode;
    }

    /**
     * Text getter
     * 
     * @return the text of the button
     */
    public String getText() {
        return btn.getText();
    }
    
    /**
     * Text setter
     * 
     * @param value the text of the button
     */
    public void setText(String value) {
        btn.setText(value);
    }

    /**
     * UserValue setter
     * 
     * @param value the value of the button
     */
    public void setUserValue(int value) {
        userValue=value;
    }

    /**
     * UserValue getter
     * 
     * @return the value of the button
     */
    public int getUserValue() {
        return userValue;
    }

    /**
     * Check the button
     */
    public void check() {
        checkIcon.setImage(checked);
        checkedProperty.set(true);
    }

    /**
     * Uncheck the button
     */
    public void uncheck() {
        checkIcon.setImage(unchecked);
        checkedProperty.set(false);
    }

    /**
     * Checked property getter
     * 
     * @return the checked property
     */
    public BooleanProperty getCheckedProperty() {
        return checkedProperty;
    }

    /**
     * Check that the button is checked
     * 
     * @return true if the button is checked
     */
    public boolean isChecked() {
        return checkedProperty.get();
    }

    /**
     * Main button handler
     */
    @FXML
    public void handleMainButton() {
        if (autoCheckMode) {
            if (checkedProperty.get()) {
                uncheck();
            } else {
                check();
            }
        }
        PrimoEvent multipleButtonEvent = new PrimoEvent();
        this.fireEvent(multipleButtonEvent);
    }

    /**
     * Mini button handler
     */
    @FXML
    public void handleMiniButton() {
        SecondoEvent multipleButtonEvent = new SecondoEvent();
        this.fireEvent(multipleButtonEvent);
    }

    /**
     * Event handler for the main action
     * 
     * @return the event handler
     */
    public final ObjectProperty<EventHandler<MultipleButtonEvent>> onMainActionProperty() { return onMainAction; }

    /**
     * Main action setter
     * 
     * @param value the event handler
     */
    public final void setOnMainAction(EventHandler<MultipleButtonEvent> value) { onMainActionProperty().set(value); }

    /**
     * Main action getter
     * 
     * @return the event handler
     */
    public final EventHandler<MultipleButtonEvent> getOnMainAction() { return onMainActionProperty().get(); }

    /**
     * Object property for the main action
     */
    private ObjectProperty<EventHandler<MultipleButtonEvent>> onMainAction = new ObjectPropertyBase<EventHandler<MultipleButtonEvent>>() {
        @Override protected void invalidated() {
            setEventHandler(PrimoEvent.ACTION, get());
        }

        @Override
        public Object getBean() {
            return btn;
        }

        @Override
        public String getName() {
            return "onMainAction";
        }
    };

    /**
     * Event handler for the info action
     * 
     * @return the event handler
     */
    public final ObjectProperty<EventHandler<MultipleButtonEvent>> onInfoActionProperty() { return onInfoAction; }

    /**
     * Info action setter
     * 
     * @param value the event handler
     */
    public final void setOnInfoAction(EventHandler<MultipleButtonEvent> value) { onInfoActionProperty().set(value); }

    /**
     * Info action getter
     * 
     * @return the event handler
     */
    public final EventHandler<MultipleButtonEvent> getOnInfoAction() { return onInfoActionProperty().get(); }

    /**
     * Object property for the info action
     */
    private ObjectProperty<EventHandler<MultipleButtonEvent>> onInfoAction = new ObjectPropertyBase<EventHandler<MultipleButtonEvent>>() {
        @Override protected void invalidated() {
            setEventHandler(SecondoEvent.ACTION, get());
        }

        @Override
        public Object getBean() {
            return miniButton;
        }

        @Override
        public String getName() {
            return "onMiniButtonAction";
        }
    };
}
