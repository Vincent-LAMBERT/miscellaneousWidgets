package miscellaneousWidgets;



import java.io.IOException;

import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ObjectPropertyBase;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;

/**
 * A button with image at its left
 */
public class NeoIconButton extends AnchorPane {
    /** The button */
    @FXML protected Button btn;
    /** The label */
    @FXML protected Label label;
    /** The anchor pane */
    @FXML protected AnchorPane internAnch;
    /** The image anchor pane */
    @FXML protected AnchorPane imgAnch;
    /** The image view */
    @FXML protected ImageView icon;
    /** The hbox */
    @FXML protected HBox hbox;


    /**
     * Constructor
     */
    public NeoIconButton() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/miscellaneousWidgets/fxml/NeoIconButton.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        
        try {
            fxmlLoader.load();            
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        DoubleBinding db = new DoubleBinding() {
            {
                super.bind(imgAnch.widthProperty(), btn.widthProperty());
            }
            @Override
            protected double computeValue() {
                return (btn.widthProperty().get() - imgAnch.widthProperty().get());
            }
        };
        icon.fitHeightProperty().bind(btn.heightProperty().subtract(26));
        icon.fitWidthProperty().bind(btn.heightProperty().subtract(26));
        label.minWidthProperty().bind(db.divide(1.25));
        label.minHeightProperty().bind(btn.heightProperty().multiply(1).subtract(4));
    }
    
    /**
     * Text getter
     * 
     * @return the text
     */
    public String getText() {
        return label.getText();
    }
    
    /**
     * Text setter
     * 
     * @param value : the text
     */
    public void setText(String value) {
        label.setText(value);
    }
    
    /**
     * Event handler for action
     * 
     * @return the object property holding the event handler
     */
    public final ObjectProperty<EventHandler<ActionEvent>> onActionProperty() { return onAction; }

    /**
     * Action setter
     * 
     * @param value : the event handler
     */
    public final void setOnAction(EventHandler<ActionEvent> value) { onActionProperty().set(value); }
    
    /**
     * On action getter
     * 
     * @return the event handler
     */
    public final EventHandler<ActionEvent> getOnAction() { return onActionProperty().get(); }

    /**
     * Object property for the action
     */
    private ObjectProperty<EventHandler<ActionEvent>> onAction = new ObjectPropertyBase<EventHandler<ActionEvent>>() {
        @Override protected void invalidated() {
            setEventHandler(ActionEvent.ACTION, get());
        }

        @Override
        public Object getBean() {
            return this;
        }

        @Override
        public String getName() {
            return "onAction";
        }
    };

    /**
     * Image property
     */
    private ObjectProperty<Image> image;

    /**
     * Image setter
     * 
     * @param image : the image
     */
    public final void setImage(Image image) {
        icon.setImage(image);
    }
    
    /**
     * Image getter
     * 
     * @return the image
     */
    public final Image getImage() {
        return icon.getImage();
    }

    /**
     * Image property setter
     * 
     * @return the object property holding the image
     */
    public final ObjectProperty<Image> imageProperty() {
        if (image == null) {
            image = new ObjectPropertyBase<Image>() {

                @Override
                public Object getBean() {
                    return icon;
                }

                @Override
                public String getName() {
                    return "image";
                }
            };
        }
        return image;
    }
}
