package miscellaneousWidgets;



import java.io.IOException;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ObjectPropertyBase;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import miscellaneousWidgets.events.CentralButtonEvent;
import miscellaneousWidgets.events.MultipleButtonEvent;
import miscellaneousWidgets.events.NeButtonEvent;
import miscellaneousWidgets.events.NwButtonEvent;
import miscellaneousWidgets.events.SeButtonEvent;
import miscellaneousWidgets.events.SwButtonEvent;

/**
 * A set of round buttons 
 */
public class NeoMultipleButtons extends AnchorPane {
    /** The main anchor pane */
    @FXML protected AnchorPane main;
    /** The anchor pane */
    @FXML protected AnchorPane pane;
    /** The stack pane */
    @FXML protected StackPane stackpane;
    /** The central buttons */
    @FXML protected Button centralButton;
    /** The south-west button */
    @FXML protected Button swButton;
    /** The south-east button */
    @FXML protected Button seButton;
    /** The north-east button */
    @FXML protected Button neButton;
    /** The north-west button */
    @FXML protected Button nwButton;
    /** The south-west first button of the duo */
    @FXML protected Button swButtonDuoOne;
    /** The south-west second button of the duo */
    @FXML protected Button swButtonDuoTwo;
    /** The south-east first button of the duo */
    @FXML protected Button seButtonDuoOne;
    /** The south-east second button of the duo */
    @FXML protected Button seButtonDuoTwo;
    /** The north-east first button of the duo */
    @FXML protected Button neButtonDuoOne;
    /** The north-east second button of the duo */
    @FXML protected Button neButtonDuoTwo;
    /** The north-west first button of the duo */
    @FXML protected Button nwButtonDuoOne;
    /** The north-west second button of the duo */
    @FXML protected Button nwButtonDuoTwo;
    /** The central image */
    @FXML protected ImageView centralImg;
    /** The south-west image */
    @FXML protected ImageView swImg;
    /** The south-east image */
    @FXML protected ImageView seImg;
    /** The north-east image */
    @FXML protected ImageView neImg;
    /** The north-west image */
    @FXML protected ImageView nwImg;
    /** The south-west first image of the duo */
    @FXML protected ImageView swImgDuoOne;
    /** The south-west second image of the duo */
    @FXML protected ImageView swImgDuoTwo;
    /** The south-east first image of the duo */
    @FXML protected ImageView seImgDuoOne;
    /** The south-east second image of the duo */
    @FXML protected ImageView seImgDuoTwo;
    /** The north-east first image of the duo */
    @FXML protected ImageView neImgDuoOne;
    /** The north-east second image of the duo */
    @FXML protected ImageView neImgDuoTwo;
    /** The north-west first image of the duo */
    @FXML protected ImageView nwImgDuoOne;
    /** The north-west second image of the duo */
    @FXML protected ImageView nwImgDuoTwo;

    /**
     * Constructor
     */
    public NeoMultipleButtons() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/miscellaneousWidgets/fxml/NeoMultipleButtons.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        
        try {
            fxmlLoader.load();            
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        ReadOnlyDoubleProperty size = main.widthProperty();
        pane.minHeightProperty().bind(size.multiply(1));
        pane.minWidthProperty().bind(size.multiply(1));
        centralButton.minHeightProperty().bind(size.multiply(0.80));
        centralButton.minWidthProperty().bind(size.multiply(0.80));
        swButton.minHeightProperty().bind(size.multiply(0.33));
        swButton.minWidthProperty().bind(size.multiply(0.33));
        seButton.minHeightProperty().bind(size.multiply(0.33));
        seButton.minWidthProperty().bind(size.multiply(0.33));
        neButton.minHeightProperty().bind(size.multiply(0.33));
        neButton.minWidthProperty().bind(size.multiply(0.33));
        nwButton.minHeightProperty().bind(size.multiply(0.33));
        nwButton.minWidthProperty().bind(size.multiply(0.33));
        swButtonDuoOne.minHeightProperty().bind(size.multiply(0.33));
        swButtonDuoOne.minWidthProperty().bind(size.multiply(0.33));
        swButtonDuoTwo.minHeightProperty().bind(size.multiply(0.33));
        swButtonDuoTwo.minWidthProperty().bind(size.multiply(0.33));
        seButtonDuoOne.minHeightProperty().bind(size.multiply(0.33));
        seButtonDuoOne.minWidthProperty().bind(size.multiply(0.33));
        seButtonDuoTwo.minHeightProperty().bind(size.multiply(0.33));
        seButtonDuoTwo.minWidthProperty().bind(size.multiply(0.33));
        neButtonDuoOne.minHeightProperty().bind(size.multiply(0.33));
        neButtonDuoOne.minWidthProperty().bind(size.multiply(0.33));
        neButtonDuoTwo.minHeightProperty().bind(size.multiply(0.33));
        neButtonDuoTwo.minWidthProperty().bind(size.multiply(0.33));
        nwButtonDuoOne.minHeightProperty().bind(size.multiply(0.33));
        nwButtonDuoOne.minWidthProperty().bind(size.multiply(0.33));
        nwButtonDuoTwo.minHeightProperty().bind(size.multiply(0.33));
        nwButtonDuoTwo.minWidthProperty().bind(size.multiply(0.33));

        Integer marginSideButtons = 10;
        Integer marginCentralButton = 15;
        centralImg.fitWidthProperty().bind(centralButton.widthProperty().subtract(marginCentralButton));
        centralImg.fitHeightProperty().bind(centralButton.heightProperty().subtract(marginCentralButton));
        swImg.fitWidthProperty().bind(swButton.widthProperty().subtract(marginSideButtons));
        swImg.fitHeightProperty().bind(swButton.heightProperty().subtract(marginSideButtons));
        seImg.fitWidthProperty().bind(seButton.widthProperty().subtract(marginSideButtons));
        seImg.fitHeightProperty().bind(seButton.heightProperty().subtract(marginSideButtons));
        nwImg.fitWidthProperty().bind(nwButton.widthProperty().subtract(marginSideButtons));
        nwImg.fitHeightProperty().bind(nwButton.heightProperty().subtract(marginSideButtons));
        neImg.fitWidthProperty().bind(neButton.widthProperty().subtract(marginSideButtons));
        neImg.fitHeightProperty().bind(neButton.heightProperty().subtract(marginSideButtons));
        swImgDuoOne.fitWidthProperty().bind(swButtonDuoOne.widthProperty().subtract(marginSideButtons));
        swImgDuoOne.fitHeightProperty().bind(swButtonDuoOne.heightProperty().subtract(marginSideButtons));
        swImgDuoTwo.fitWidthProperty().bind(swButtonDuoTwo.widthProperty().subtract(marginSideButtons));
        swImgDuoTwo.fitHeightProperty().bind(swButtonDuoTwo.heightProperty().subtract(marginSideButtons));
        seImgDuoOne.fitWidthProperty().bind(seButtonDuoOne.widthProperty().subtract(marginSideButtons));
        seImgDuoOne.fitHeightProperty().bind(seButtonDuoOne.heightProperty().subtract(marginSideButtons));
        seImgDuoTwo.fitWidthProperty().bind(seButtonDuoTwo.widthProperty().subtract(marginSideButtons));
        seImgDuoTwo.fitHeightProperty().bind(seButtonDuoTwo.heightProperty().subtract(marginSideButtons));
        neImgDuoOne.fitWidthProperty().bind(neButtonDuoOne.widthProperty().subtract(marginSideButtons));
        neImgDuoOne.fitHeightProperty().bind(neButtonDuoOne.heightProperty().subtract(marginSideButtons));
        neImgDuoTwo.fitWidthProperty().bind(neButtonDuoTwo.widthProperty().subtract(marginSideButtons));
        neImgDuoTwo.fitHeightProperty().bind(neButtonDuoTwo.heightProperty().subtract(marginSideButtons));
        nwImgDuoOne.fitWidthProperty().bind(nwButtonDuoOne.widthProperty().subtract(marginSideButtons));
        nwImgDuoOne.fitHeightProperty().bind(nwButtonDuoOne.heightProperty().subtract(marginSideButtons));
        nwImgDuoTwo.fitWidthProperty().bind(nwButtonDuoTwo.widthProperty().subtract(marginSideButtons));
        nwImgDuoTwo.fitHeightProperty().bind(nwButtonDuoTwo.heightProperty().subtract(marginSideButtons));
        centralButton.setPickOnBounds(false);
        swButton.setPickOnBounds(false);
        seButton.setPickOnBounds(false);
        neButton.setPickOnBounds(false);
        nwButton.setPickOnBounds(false);
        swButtonDuoOne.setPickOnBounds(false);
        swButtonDuoTwo.setPickOnBounds(false);
        seButtonDuoOne.setPickOnBounds(false);
        seButtonDuoTwo.setPickOnBounds(false);
        neButtonDuoOne.setPickOnBounds(false);
        neButtonDuoTwo.setPickOnBounds(false);
        nwButtonDuoOne.setPickOnBounds(false);
        nwButtonDuoTwo.setPickOnBounds(false);
    }

    /**
     * Set the visibility of the central button
     * 
     * @param bool : true if visible
     */
    public void setVisibleCentral(boolean bool) {
        centralButton.setVisible(bool);
    }

    /**
     * Set the visibility of the south-west button
     * 
     * @param bool : true if visible
     */
    public void setVisibleSw(boolean bool) {
        swButton.setVisible(bool);
    }

    /**
     * Set the visibility of the south-east button
     * 
     * @param bool : true if visible
     */
    public void setVisibleSe(boolean bool) {
        seButton.setVisible(bool);
    }

    /**
     * Set the visibility of the north-east button
     * 
     * @param bool : true if visible
     */
    public void setVisibleNe(boolean bool) {
        neButton.setVisible(bool);
    }

    /**
     * Set the visibility of the north-west button
     * 
     * @param bool : true if visible
     */
    public void setVisibleNw(boolean bool) {
        nwButton.setVisible(bool);
    }

    /**
     * Set the visibility of the south-west duo
     * 
     * @param bool : true if visible
     */
    public void setVisibleSwDuo(boolean bool) {
        swButtonDuoOne.setVisible(bool);
        swButtonDuoTwo.setVisible(bool);
    }

    /**
     * Set the visibility of the south-east duo
     * 
     * @param bool : true if visible
     */
    public void setVisibleSeDuo(boolean bool) {
        seButtonDuoOne.setVisible(bool);
        seButtonDuoTwo.setVisible(bool);
    }

    /**
     * Set the visibility of the north-east duo
     * 
     * @param bool : true if visible
     */
    public void setVisibleNeDuo(boolean bool) {
        neButtonDuoOne.setVisible(bool);
        neButtonDuoTwo.setVisible(bool);
    }

    /**
     * Set the visibility of the north-west duo
     * 
     * @param bool : true if visible
     */
    public void setVisibleNwDuo(boolean bool) {
        nwButtonDuoOne.setVisible(bool);
        nwButtonDuoTwo.setVisible(bool);
    }

    /**
     * Check if the central button is visible
     * 
     * @return true if visible
     */
    public boolean isVisibleCentral() {
        return centralButton.isVisible();
    }

    /**
     * Check if the south-west button is visible
     * 
     * @return true if visible
     */
    public boolean isVisibleSw() {
        return swButton.isVisible();
    }

    /**
     * Check if the south-east button is visible
     * 
     * @return true if visible
     */
    public boolean isVisibleSe() {
        return seButton.isVisible();
    }

    /**
     * Check if the north-east button is visible
     * 
     * @return true if visible
     */
    public boolean isVisibleNe() {
        return neButton.isVisible();
    }

    /**
     * Check if the north-west button is visible
     * 
     * @return true if visible
     */
    public boolean isVisibleNw() {
        return nwButton.isVisible();
    }

    /**
     * Check if the south-west duo is visible
     * 
     * @return true if visible
     */
    public boolean isVisibleSwDuo() {
        return swButtonDuoOne.isVisible();
    }

    /**
     * Check if the south-east duo is visible
     * 
     * @return true if visible
     */
    public boolean isVisibleSeDuo() {
        return seButtonDuoOne.isVisible();
    }

    /**
     * Check if the north-east duo is visible
     * 
     * @return true if visible
     */
    public boolean isVisibleNeDuo() {
        return neButtonDuoOne.isVisible();
    }

    /**
     * Check if the north-west duo is visible
     * 
     * @return true if visible
     */
    public boolean isVisibleNwDuo() {
        return nwButtonDuoOne.isVisible();
    }
    
    /**
     * Set the central button image
     * 
     * @param img : the image
     */
    public void setCentralImage(Image img) {
        centralImg.setImage(img);
    }
    
    /**
     * Set the south-west button image
     * 
     * @param img : the image
     */
    public void setSwImage(Image img) {
        swImg.setImage(img);
    }
    
    /**
     * Set the south-east button image
     * 
     * @param img : the image
     */
    public void setSeImage(Image img) {
        seImg.setImage(img);
    }
    
    /**
     * Set the north-east button image
     * 
     * @param img : the image
     */
    public void setNeImage(Image img) {
        neImg.setImage(img);
    }
    
    /**
     * Set the north-west button image
     * 
     * @param img : the image
     */
    public void setNwImage(Image img) {
        nwImg.setImage(img);
    }

    /**
     * Set the south-west first button of the duo image
     * 
     * @param img : the image
     */
    public void setSwImageDuoOne(Image img) {
        swImgDuoOne.setImage(img);
    }

    /**
     * Set the south-west second button of the duo image
     * 
     * @param img : the image
     */
    public void setSwImageDuoTwo(Image img) {
        swImgDuoTwo.setImage(img);
    }

    /**
     * Set the south-east first button of the duo image
     * 
     * @param img : the image
     */
    public void setSeImageDuoOne(Image img) {
        seImgDuoOne.setImage(img);
    }

    /**
     * Set the south-east second button of the duo image
     * 
     * @param img : the image
     */
    public void setSeImageDuoTwo(Image img) {
        seImgDuoTwo.setImage(img);
    }

    /**
     * Set the north-east first button of the duo image
     * 
     * @param img : the image
     */
    public void setNeImageDuoOne(Image img) {
        neImgDuoOne.setImage(img);
    }

    /**
     * Set the north-east second button of the duo image
     * 
     * @param img : the image
     */
    public void setNeImageDuoTwo(Image img) {
        neImgDuoTwo.setImage(img);
    }

    /**
     * Set the north-west first button of the duo image
     * 
     * @param img : the image
     */
    public void setNwImageDuoOne(Image img) {
        nwImgDuoOne.setImage(img);
    }

    /**
     * Set the north-west second button of the duo image
     * 
     * @param img : the image
     */
    public void setNwImageDuoTwo(Image img) {
        nwImgDuoTwo.setImage(img);
    }

    /**
     * Handle the central button
     */
    @FXML
    public void handleCentralButton() {
        CentralButtonEvent multEvent = new CentralButtonEvent();
        this.fireEvent(multEvent);
    }

    /**
     * Handle the south-west button
     */
    @FXML
    public void handleSwButton() {
        SwButtonEvent multEvent = new SwButtonEvent();
        this.fireEvent(multEvent);
    }

    /**
     * Handle the south-east button
     */
    @FXML
    public void handleSeButton() {
        SeButtonEvent multEvent = new SeButtonEvent();
        this.fireEvent(multEvent);
    }

    /**
     * Handle the north-east button
     */
    @FXML
    public void handleNeButton() {
        NeButtonEvent multEvent = new NeButtonEvent();
        this.fireEvent(multEvent);
    }

    /**
     * Handle the north-west button
     */
    @FXML
    public void handleNwButton() {
        NwButtonEvent multEvent = new NwButtonEvent();
        this.fireEvent(multEvent);
    }

    /**
     * Handle the south-west duo button
     */
    @FXML
    public void handleSwDuoButton() {
        SwButtonEvent multEvent = new SwButtonEvent();
        this.fireEvent(multEvent);
    }

    /**
     * Handle the south-east duo button
     */
    @FXML
    public void handleSeDuoButton() {
        SeButtonEvent multEvent = new SeButtonEvent();
        this.fireEvent(multEvent);
    }

    /**
     * Handle the north-east duo button
     */
    @FXML
    public void handleNeDuoButton() {
        NeButtonEvent multEvent = new NeButtonEvent();
        this.fireEvent(multEvent);
    }

    /**
     * Handle the north-west duo button
     */
    @FXML
    public void handleNwDuoButton() {
        NwButtonEvent multEvent = new NwButtonEvent();
        this.fireEvent(multEvent);
    }

    /**
     * Event handler for the central button
     * 
     * @return the object property holding the event handler
     */
    public final ObjectProperty<EventHandler<MultipleButtonEvent>> onCentralActionProperty() { return onCentralAction; }
    
    /**
     * Central action setter
     * 
     * @param value : the event handler
     */
    public final void setOnCentralAction(EventHandler<MultipleButtonEvent> value) { onCentralActionProperty().set(value); }

    /**
     * Central action getter
     * 
     * @return the event handler
     */
    public final EventHandler<MultipleButtonEvent> getOnCentralAction() { return onCentralActionProperty().get(); }

    /**
     * Object property for the central action
     */
    private ObjectProperty<EventHandler<MultipleButtonEvent>> onCentralAction = new ObjectPropertyBase<EventHandler<MultipleButtonEvent>>() {
        @Override protected void invalidated() {
            setEventHandler(CentralButtonEvent.ACTION, get());
        }

        @Override
        public Object getBean() {
            return this;
        }

        @Override
        public String getName() {
            return "onCentralAction";
        }
    };

    /**
     * Event handler for the south-west button
     * 
     * @return the object property holding the event handler
     */
    public final ObjectProperty<EventHandler<MultipleButtonEvent>> onSwActionProperty() { return onSwAction; }

    /**
     * South-west action setter
     * 
     * @param value : the event handler
     */
    public final void setOnSwAction(EventHandler<MultipleButtonEvent> value) { onSwActionProperty().set(value); }

    /**
     * South-west action getter
     * 
     * @return the event handler
     */
    public final EventHandler<MultipleButtonEvent> getOnSwAction() { return onSwActionProperty().get(); }

    /**
     * Object property for the south-west action
     */
    private ObjectProperty<EventHandler<MultipleButtonEvent>> onSwAction = new ObjectPropertyBase<EventHandler<MultipleButtonEvent>>() {
        @Override protected void invalidated() {
            setEventHandler(SwButtonEvent.ACTION, get());
        }

        @Override
        public Object getBean() {
            return this;
        }

        @Override
        public String getName() {
            return "onSwAction";
        }
    };

    /**
     * Event handler for the south-east button
     * 
     * @return the object property holding the event handler
     */
    public final ObjectProperty<EventHandler<MultipleButtonEvent>> onSeActionProperty() { return onSeAction; }

    /**
     * South-east action setter
     * 
     * @param value : the event handler
     */
    public final void setOnSeAction(EventHandler<MultipleButtonEvent> value) { onSeActionProperty().set(value); }

    /**
     * South-east action getter
     * 
     * @return the event handler
     */
    public final EventHandler<MultipleButtonEvent> getOnSeAction() { return onSeActionProperty().get(); }

    /**
     * Object property for the south-east action
     */
    private ObjectProperty<EventHandler<MultipleButtonEvent>> onSeAction = new ObjectPropertyBase<EventHandler<MultipleButtonEvent>>() {
        @Override protected void invalidated() {
            setEventHandler(SeButtonEvent.ACTION, get());
        }

        @Override
        public Object getBean() {
            return this;
        }

        @Override
        public String getName() {
            return "onSeAction";
        }
    };

    /**
     * Event handler for the north-east button
     * 
     * @return the object property holding the event handler
     */
    public final ObjectProperty<EventHandler<MultipleButtonEvent>> onNeActionProperty() { return onNeAction; }

    /**
     * North-east action setter
     * 
     * @param value : the event handler
     */
    public final void setOnNeAction(EventHandler<MultipleButtonEvent> value) { onNeActionProperty().set(value); }

    /**
     * North-east action getter
     * 
     * @return the event handler
     */
    public final EventHandler<MultipleButtonEvent> getOnNeAction() { return onNeActionProperty().get(); }

    /**
     * Object property for the north-east action
     */
    private ObjectProperty<EventHandler<MultipleButtonEvent>> onNeAction = new ObjectPropertyBase<EventHandler<MultipleButtonEvent>>() {
        @Override protected void invalidated() {
            setEventHandler(NeButtonEvent.ACTION, get());
        }

        @Override
        public Object getBean() {
            return this;
        }

        @Override
        public String getName() {
            return "onNeAction";
        }
    };

    /**
     * Event handler for the north-west button
     * 
     * @return the object property holding the event handler
     */
    public final ObjectProperty<EventHandler<MultipleButtonEvent>> onNwActionProperty() { return onNwAction; }

    /**
     * North-west action setter
     * 
     * @param value : the event handler
     */
    public final void setOnNwAction(EventHandler<MultipleButtonEvent> value) { onNwActionProperty().set(value); }

    /**
     * North-west action getter
     * 
     * @return the event handler
     */
    public final EventHandler<MultipleButtonEvent> getOnNwAction() { return onNwActionProperty().get(); }

    /**
     * Object property for the north-west action
     */
    private ObjectProperty<EventHandler<MultipleButtonEvent>> onNwAction = new ObjectPropertyBase<EventHandler<MultipleButtonEvent>>() {
        @Override protected void invalidated() {
            setEventHandler(NwButtonEvent.ACTION, get());
        }

        @Override
        public Object getBean() {
            return this;
        }

        @Override
        public String getName() {
            return "onNwAction";
        }
    };
}
