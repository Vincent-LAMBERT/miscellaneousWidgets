package miscellaneousWidgets;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;

/**
 * A custom progess indicator
 */
public class NeoProgressIndicator extends AnchorPane {
    double progress1 = 0.75;
    double progress = 270;
    Color strokeCol = Color.RED;
    @FXML
    Arc arc;

    /**
     * Constructor
     */
    public NeoProgressIndicator() {
        FXMLLoader fxmlLoader = new FXMLLoader(
                getClass().getResource("/miscellaneousWidgets/fxml/NeoProgressIndicator.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        actualize();
    }

    /**
     * Progress setter
     * 
     * @param progress_value : the progress value (between 0 and 1)
     */
    public void setProgress(double progress_value) {
        progress1 = progress_value;
        progress = progress_value * 360;
        actualize();
    }

    /**
     * Progress getter
     * 
     * @return the progress value (between 0 and 1)
     */
    public double getProgress() {
        return progress1;
    }

    /**
     * Color setter
     * 
     * @param col : the color
     */
    public void setColor(Color col) {
        strokeCol = col;
        actualize();
    }

    /**
     * Actualize the progress indicator
     */
    private void actualize() {
        arc.setLength(progress);
        arc.setStroke(strokeCol);
    }
}
