package miscellaneousWidgets.events;

import javafx.event.Event;
import javafx.event.EventType;

/**
 * A custom event for complex widgets made of multiple buttons
 */
public abstract class MultipleButtonEvent extends Event {

    private static final long serialVersionUID = 1L;
    /** The event type */
	public static final EventType<MultipleButtonEvent> CUSTOM_EVENT_TYPE = new EventType<MultipleButtonEvent>(ANY);

    /**
     * Constructor
     * 
     * @param eventType the event type
     */
    public MultipleButtonEvent(EventType<? extends Event> eventType) {
        super(eventType);
    }

    /**
     * Handler invoker
     * 
     * @param handler the handler
     */
    public abstract void invokeHandler(MultipleButtonEventHandler handler);

}
