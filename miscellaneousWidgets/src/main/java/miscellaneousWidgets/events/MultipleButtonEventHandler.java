package miscellaneousWidgets.events;

import javafx.event.EventHandler;

/**
 * Multiple button event handler
 */
public abstract class MultipleButtonEventHandler implements EventHandler<MultipleButtonEvent> {

    /**
     * Constructor
     */
    public MultipleButtonEventHandler() {}

    /**
     * On event
     */
    public abstract void onEvent();

    /**
     * Handler invoker
     * 
     * @param event the event
     */
    @Override
    public void handle(MultipleButtonEvent event) {
        event.invokeHandler(this);
    }
}