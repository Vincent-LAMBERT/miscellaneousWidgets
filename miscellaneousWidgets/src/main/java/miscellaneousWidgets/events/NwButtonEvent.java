package miscellaneousWidgets.events;

import javafx.event.EventType;

/**
 * A custom event for the north-west button
 */
public class NwButtonEvent extends MultipleButtonEvent {

    private static final long serialVersionUID = 1L;
    /** The event type */
	public static final EventType<MultipleButtonEvent> ACTION = new EventType<MultipleButtonEvent>(CUSTOM_EVENT_TYPE, "NwButtonEvent");


    /**
     * Constructor
     */
    public NwButtonEvent() {
        super(ACTION);
    }

    /**
     * Handler invoker
     * 
     * @param handler the handler
     */
    @Override
    public void invokeHandler(MultipleButtonEventHandler handler) {
        handler.onEvent();
    }

}