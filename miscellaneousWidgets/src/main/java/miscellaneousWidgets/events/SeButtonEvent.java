package miscellaneousWidgets.events;

import javafx.event.EventType;

/**
 * A custom event for the south-east button
 */
public class SeButtonEvent extends MultipleButtonEvent {

    private static final long serialVersionUID = 1L;
    /** The event type */
	public static final EventType<MultipleButtonEvent> ACTION = new EventType<MultipleButtonEvent>(CUSTOM_EVENT_TYPE, "SeButtonEvent");

    /**
     * Constructor
     */
    public SeButtonEvent() {
        super(ACTION);
    }

    /**
     * Handler invoker
     * 
     * @param handler the handler
     */
    @Override
    public void invokeHandler(MultipleButtonEventHandler handler) {
        handler.onEvent();
    }

}