/** 
 * Module declaration for the miscellaneousWidgets module.
 */
module miscellaneousWidgets {
    requires javafx.base;
    requires transitive javafx.controls;
    requires transitive javafx.graphics;
    requires javafx.fxml;

    opens miscellaneousWidgets to javafx.fxml;
    exports miscellaneousWidgets;
    opens miscellaneousWidgets.events to javafx.fxml;
    exports miscellaneousWidgets.events;
}
