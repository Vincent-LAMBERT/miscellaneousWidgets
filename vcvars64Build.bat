@ECHO OFF
:: This batch file launches maven for Windows
TITLE Maven build for Windows
:: Section 1: Windows 10 information
ECHO ==========================
ECHO Getting the path
SET BUILD_PATH=%~dp0
ECHO %BUILD_PATH:~0,-1%
ECHO ==========================
ECHO Changing directory
CD %BUILD_PATH%
ECHO ==========================
ECHO Launching Gradle
@REM @REM Clean
@REM gradlew.bat clean 
@REM @REM build
@REM gradlew.bat build
@REM @REM publishToMavenLocal
@REM gradlew.bat publishToMavenLocal
@REM All
gradlew.bat clean build publishToMavenLocal